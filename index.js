function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        var btn_create = document.getElementById("create-room");
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logoutbtn = document.getElementById('logout-btn');
            logoutbtn.addEventListener("click", function() {
                firebase.auth().signOut().then(function() {
                    alert("User sign out success!");
                    window.location.href = "index.html";
                }).catch(function(error) {
                    alert("User sign out failed!");
                })
            }, false);
            //create-chatroom
            
        } else {b
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');


    // Trace the code first! There are some adjustments...
    post_btn.addEventListener('click', function() {
        if (post_txt.value != "") {
            var Ref = firebase.database().ref('com_list');
            console.log("test");
            var data = {
                email: user_email,
                // Type 0 for comment
                type: 0,
                data: post_txt.value,
                url: ''
            };
            Ref.push(data);
            post_txt.value = "";
        }
    });
    
    
    img_btn = document.getElementById('img_btn');

    /// TODO 2: Put the image to storage, and push the image to database's "com_list" node
    ///         1. Get the reference of firebase storage
    ///         2. Upload the image
    ///         3. Get the image file url, the reference of "com_list" and push user email and the image
    img_btn.addEventListener('change', function(){
        var file = this.files[0];
        console.log(file);
        var storageRef = firebase.storage().ref(file.name);
        storageRef.put(file).then(function(){
            storageRef.getDownloadURL().then(function(url){
                console.log(url);
                var Ref = firebase.database().ref('com_list');
                var data = {
                    email: user_email,
                    // Type 1 for image
                    type: 1,
                    data: '',
                    url: url
                };
                Ref.push(data);
            });
        });
    });


    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";
    var str_before_img = "<img class='img pt-2' style='height: 300px;' src='";
    var str_after_img = "'>";

    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function(snapshot) {
            /// TODO 3: Adjust the history posts part and the new post part to display both comment and image
            ///         1. Use str_before_img, str_after_img to form the HTML part.
            ///
            ///         Hint: check those we push into the database, some datas can help you.
            snapshot.forEach(function(childshot) {
                var childData = childshot.val();
                if(childData.type === 0) 
                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                else
                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + str_after_content;
                first_count += 1;
            });

            /// Join all post in list to html in once
            document.getElementById('post_list').innerHTML = total_post.join('');

            /// Add listener to update new post
            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    if (childData.type === 0) 
                        total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                    else 
                        total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
}

window.onload = function() {
    init();
};